import socket

TCP_IP = '127.0.0.1'
TCP_PORT = 5005

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)

    while(True):
        (conn, addr) = s.accept()

        print('Server Address:', TCP_IP)
        print('Client Address:', addr)
        print("Connection to Client Established, Waiting to Receive Message...")


        while(True):
            data = conn.recv(1024)

            print(data)

            test = "test".encode()
            s.send(test)



s.close()
