import socket
import time

from datetime import datetime, date, time
 

localIP     = "127.0.0.1"

localPort   = 20001

bufferSize  = 1024

 

msgFromServer       = ""



def time1date():
    currDateTime = datetime.now()
    stringToReturn = currDateTime.strftime("%m/%d/%y %H:%M:%S")
    return "Current Date and Time - " + stringToReturn




 

# Create a datagram socket

UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

 

# Bind to address and ip

UDPServerSocket.bind((localIP, localPort))

 

print("UDP server up and listening")

 

# Listen for incoming datagrams

while(True):

    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)

    message = bytesAddressPair[0]

    address = bytesAddressPair[1]

    clientMsg = "Message from Client:{}".format(message)

    clientIP  = "Client IP Address:{}".format(address)

    print(clientMsg)
    print(clientIP)

   

    # Sending a reply to client

    if (message.decode() == "What is the current date and time?"):
        bytesToSend = str.encode(time1date())

    else:
        bytesToSend = str.encode("Invalid Request")

    UDPServerSocket.sendto(bytesToSend, address)
